﻿// EventPattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using DustInTheWind.EventPattern.Numbers;

namespace DustInTheWind.EventPattern
{
    internal class Program
    {
        private static void Main()
        {
            RandomNumbers numbers = new RandomNumbers();

            // (1) Subscribe to the event.
            numbers.NumberGenerated += HandleNumberGenerated;

            numbers.Generate(10);

            Pause();
        }

        public static void HandleNumberGenerated(object sender, NumberGeneratedEventArgs e)
        {
            // (3) Handle the event.

            DateTime generatedTime = e.GeneratedTime;
            int number = e.Number;

            Console.WriteLine($"[{generatedTime:dd-MM-yyyy HH:mm:ss.fff}] {number}");
        }

        private static void Pause()
        {
            Console.WriteLine();
            Console.Write("Press any key to end the program...");
            Console.ReadKey(true);
        }
    }
}