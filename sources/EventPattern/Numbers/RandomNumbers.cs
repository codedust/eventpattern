﻿// EventPattern
// Copyright (C) 2019 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Threading;

namespace DustInTheWind.EventPattern.Numbers
{
    /// <summary>
    /// This class is generating random numbers.
    /// Each time a number is generated, it raises the NumberGenerated event to announce the number.
    /// </summary>
    internal class RandomNumbers
    {
        private readonly Random random = new Random();

        /// <summary>
        /// In this example I followed the pattern from Microsoft and created the event of type <see cref="EventHandler{TEventArgs}"/>.
        /// This is not mandatory. If needed, I can use any delegate here, instead of the <see cref="EventHandler{TEventArgs}"/>.
        /// </summary>
        public event EventHandler<NumberGeneratedEventArgs> NumberGenerated;

        public void Generate(int count)
        {
            for (int i = 0; i < count; i++)
            {
                int number = random.Next();
                DateTime generatedTime = DateTime.Now;
                NumberGeneratedEventArgs numberGeneratedEventArgs = new NumberGeneratedEventArgs(generatedTime, number);

                // (2) Raise the event.
                OnNumberGenerated(numberGeneratedEventArgs);

                Thread.Sleep(100);
            }
        }

        protected virtual void OnNumberGenerated(NumberGeneratedEventArgs e)
        {
            NumberGenerated?.Invoke(this, e);
        }
    }
}